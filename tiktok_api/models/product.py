from odoo import fields , api , models

class product(models.Model):

    _name = 'product_tiktok'
    _description = 'product.tiktok'

    name = fields.Char(string = 'Nhập tên sản phẩm :',required=True)
    image = fields.Binary(string = 'Chèn hình ảnh .' , attachment = True)
    description = fields.Char(string = 'Mô tả sản phẩm :')
    trademark = fields.Selection([
        ('nuoc ngoai', 'Nước Ngoài'),
        ('trong nuoc', 'Trong Nước'),
    ],string = 'Thương Hiệu :')
    attribute_id = fields.Char(string = 'Tên biến thể :')
    attribute_line_ids = fields.Selection([
        ('size s', 'Size S'),
        ('size m', 'Size M'),
        ('size l', 'Size L'),
    ],string  = 'Tùy Chọn')
    list_price = fields.Float(string = 'Giá bán :')
    quantily = fields.Float(string = 'Số lượng tồn kho :')
    weight = fields.Float(string = 'Trọng lượng sản phẩm')



